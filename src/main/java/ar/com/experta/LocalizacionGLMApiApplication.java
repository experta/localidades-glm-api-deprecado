package ar.com.experta;

import ar.com.experta.exceptions.MapperExceptionMapper;
import ar.com.experta.exceptions.NotFoundExceptionMapper;
import ar.com.experta.exceptions.SQLExceptionMapper;
import ar.com.experta.resources.CodigoPostalResource;
import ar.com.experta.resources.LocalizacionResource;
import ar.com.experta.resources.NacionalidadResource;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.mtakaki.dropwizard.petite.PetiteBundle;
import com.github.mtakaki.dropwizard.petite.PetiteConfiguration;
import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.jdbi.v3.core.Jdbi;
import org.marmelo.dropwizard.metrics.bundles.MetricsUIBundle;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class LocalizacionGLMApiApplication extends Application<LocalizacionGLMApiConfiguration> {

    private final SwaggerBundle swagger = new SwaggerBundle<LocalizacionGLMApiConfiguration>() {
        @Override
        protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(LocalizacionGLMApiConfiguration configuration) {
            return configuration.getSwaggerBundleConfiguration();
        }
    };

    private final PetiteBundle<LocalizacionGLMApiConfiguration> petiteBundle = new PetiteBundle<LocalizacionGLMApiConfiguration>() {
        @Override
        protected PetiteConfiguration getConfiguration(final LocalizacionGLMApiConfiguration configuration) {
            return configuration.getPetite();
        }
    };


    public static void main(final String[] args) throws Exception {
        new LocalizacionGLMApiApplication().run(args);
    }

    @Override
    public String getName() {
        return "localidades-glm-api";
    }

    @Override
    public void initialize(final Bootstrap<LocalizacionGLMApiConfiguration> bootstrap) {
        
        bootstrap.addBundle(this.petiteBundle);
        bootstrap.addBundle(new MetricsUIBundle());
		bootstrap.addBundle(this.swagger);
    }

    @Override
    public void run(final LocalizacionGLMApiConfiguration configuration,
                    final Environment environment) {
        configurarJackson(environment);
        configurarCrossOrigin(environment);
        configurarExceptionMappers(environment);
        this.jdbiConfiguration(environment, configuration);

        environment.jersey().register(this.petiteBundle.getPetiteContainer().getBean(CodigoPostalResource.class));
        environment.jersey().register(this.petiteBundle.getPetiteContainer().getBean(LocalizacionResource.class));
        environment.jersey().register(this.petiteBundle.getPetiteContainer().getBean(NacionalidadResource.class));

    }

    private void configurarExceptionMappers(Environment environment) {
        environment.jersey().register(new JsonProcessingExceptionMapper(true));
        environment.jersey().register(new NotFoundExceptionMapper(environment.metrics()));
        environment.jersey().register(new SQLExceptionMapper(environment.metrics()));
        environment.jersey().register(new MapperExceptionMapper(environment.metrics()));
    }

    private void configurarJackson(Environment environment) {

        ObjectMapper objectMapper = environment.getObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        objectMapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    }

    private void configurarCrossOrigin(Environment environment) {

        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Access-Control-Allow-Origin,Accept,Origin,Authorization");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD,PATCH");
        cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
        cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, Boolean.FALSE.toString());

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    }

    private void jdbiConfiguration(Environment environment, LocalizacionGLMApiConfiguration configuration) {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSource(), "datasource");
        this.petiteBundle.getPetiteContainer().addBean("jdbi", jdbi);
    }

}
