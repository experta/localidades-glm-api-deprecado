package ar.com.experta;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.github.mtakaki.dropwizard.petite.PetiteConfiguration;

public class LocalizacionGLMApiConfiguration extends Configuration {

    private final PetiteConfiguration petite = new PetiteConfiguration();

    @Valid
    @NotNull
    private SwaggerBundleConfiguration swaggerBundleConfiguration;

    @JsonProperty("swagger")
    public SwaggerBundleConfiguration getSwaggerBundleConfiguration() {
        return swaggerBundleConfiguration;
    }

    @Valid
    @NotNull
    @JsonProperty("database")
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("database")
    public DataSourceFactory getDataSource() {
        return database;
    }

    public PetiteConfiguration getPetite() {
        return petite;
    }
}
