package ar.com.experta.api;

import io.swagger.annotations.ApiModel;

/**
 * DTO para localidad
 */
@ApiModel(value = "Localizacion")
public class LocalizacionDTO {

    private String idLocalizacion;
    private String codigoPostal;
    private String subcp;
    private String localidad;
    private String provincia;
    private String zonaHogar;

    public LocalizacionDTO(String idLocalizacion, String codigoPostal, String subcp, String localidad, String provincia, String zonaHogar) {
        this.idLocalizacion = idLocalizacion;
        this.codigoPostal = codigoPostal;
        this.subcp = subcp;
        this.localidad = localidad;
        this.provincia = provincia;
        this.zonaHogar = zonaHogar;
    }

    public String getIdLocalizacion() {
        return idLocalizacion;
    }

    public void setIdLocalizacion(String idLocalizacion) {
        this.idLocalizacion = idLocalizacion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getSubcp() {
        return subcp;
    }

    public void setSubcp(String subcp) {
        this.subcp = subcp;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getZonaHogar() {
        return zonaHogar;
    }

    public void setZonaHogar(String zonaHogar) {
        this.zonaHogar = zonaHogar;
    }
}


