package ar.com.experta.api;

public enum Rama {
    AP("7"),
    HOGAR("15"),
    AUTOS("4"),
    OTROS("99");

    private String codigoGlm;

    Rama(String codigoGlm) {
        this.codigoGlm = codigoGlm;
    }

    public String codigoGlm() {
        return codigoGlm;
    }
}
