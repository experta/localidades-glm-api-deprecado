package ar.com.experta.core;

import ar.com.experta.api.LocalizacionDTO;
import ar.com.experta.api.Rama;
import ar.com.experta.db.LocalizacionDAO;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

import java.util.List;

@PetiteBean
public class CodigoPostalService {
    
    private final LocalizacionDAO codPostalDao;

    @PetiteInject
    public CodigoPostalService(LocalizacionDAO codPostalDao) {
        this.codPostalDao = codPostalDao;
    }

    /**
     * Busca localidades por codigo postal
     * @param codigoPostal
     * @return - Lista de localidades para ese codigo postal
     */
    public List<LocalizacionDTO> buscarLocalizacionPorCP(String codigoPostal) {
        return codPostalDao.buscarLocalizaciones(null,codigoPostal.toString());
    }

    /**
     *
     * @param id
     * @return
     */
    public List<LocalizacionDTO> buscarLocalizacionPorConsecutivo(String id) {
        return codPostalDao.buscarLocalizaciones(id, null);
    }
}
