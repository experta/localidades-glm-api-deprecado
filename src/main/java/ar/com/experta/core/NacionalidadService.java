package ar.com.experta.core;

import ar.com.experta.api.Nacionalidad;
import ar.com.experta.db.NacionalidadDAO;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

import java.util.List;

@PetiteBean
public class NacionalidadService {

    private final NacionalidadDAO nacionalidadDAO;

    @PetiteInject
    public NacionalidadService(NacionalidadDAO nacionalidadDAO) {
        this.nacionalidadDAO = nacionalidadDAO;
    }

    public List<Nacionalidad> buscarNacionalidades() {
        return nacionalidadDAO.buscarNacionalidades();
    }

}
