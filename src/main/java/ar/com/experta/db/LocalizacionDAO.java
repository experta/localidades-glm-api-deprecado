package ar.com.experta.db;

import ar.com.experta.api.LocalizacionDTO;
import ar.com.experta.api.Rama;
import ar.com.experta.db.mapper.LocalizacionDTOMapper;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@PetiteBean
public class LocalizacionDAO {
    private static final Logger logger = LoggerFactory.getLogger(LocalizacionDAO.class);

    private final Jdbi jdbi;
    private final LocalizacionDTOMapper localizacionDTOMapper;

    @PetiteInject
    public LocalizacionDAO(Jdbi jdbi, LocalizacionDTOMapper localizacionDTOMapper) {
        this.jdbi = jdbi;
        this.localizacionDTOMapper = localizacionDTOMapper;
    }

    /**
     * Buscar localidades por codigo postal
     *
     * @param codPostal - numero de codigo postal
     * @return - lista de localidades para eses codigo postal.
     */
    public List<LocalizacionDTO> buscarLocalizaciones(String localizacion, String codPostal) {
        List<LocalizacionDTO> listaLocalizaciones = new ArrayList<>();

        Integer subcp = null;
        if (localizacion != null){
            codPostal = localizacion.substring(0,4);
            subcp = Integer.valueOf(localizacion.substring(4,7));
        }

        try (Handle handle = jdbi.open()) {
             listaLocalizaciones = handle.createQuery(  " SELECT cps.cp || LPAD(cps.subcp,3,'0') localizacion, " +
                                                             "       cps.cp cp, " +
                                                             "       cps.subcp subcp, " +
                                                             "       cps.localidad localidad, " +
                                                             "       cps.provincia provincia, " +
                                                             "       zongeo1.zoncod zonahogar " +
                                                             " FROM ( SELECT codpos.cpcod cp, " +
                                                             "               MIN(codpos.cpsub) subcp, " +
                                                             "               codpos.cploc localidad, " +
                                                             "               provin.prvnom provincia " +
                                                             "          FROM glm_seguros.codpos  " +
                                                             "        INNER JOIN glm_seguros.provin ON codpos.prvcod = provin.prvcod " +
                                                             "        WHERE NVL(:cod_postal, codpos.cpcod) = codpos.cpcod " +
                                                             "        AND NVL(:sub_cp, codpos.cpsub) = codpos.cpsub " +
                                                             "        GROUP BY codpos.cploc, codpos.cpcod, provin.prvnom " +
                                                             "      ) cps       " +
                                                             " LEFT OUTER JOIN glm_seguros.zongeo1 ON zoncodpos = cps.cp AND zonsubpos = cps.subcp " +
                                                             " WHERE 15 = zongeo1.ramcod " )
                     .bind("cod_postal", codPostal)
                     .bind("sub_cp", subcp)
                     .map(localizacionDTOMapper)
                     .list();
        } catch (Exception e) {
            logger.error("Error SQL consulta localidades", e);
        }
        return listaLocalizaciones;
    }

}
