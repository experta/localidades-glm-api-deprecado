package ar.com.experta.db;

import ar.com.experta.api.Nacionalidad;
import ar.com.experta.db.mapper.NacionalidadMapper;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.List;

@PetiteBean
public class NacionalidadDAO {
    private static final Logger logger = LoggerFactory.getLogger(NacionalidadDAO.class);

    private final Jdbi jdbi;
    private final NacionalidadMapper nacionalidadMapper;

    @PetiteInject
    public NacionalidadDAO(Jdbi jdbi, NacionalidadMapper nacionalidadMapper) {
        this.jdbi = jdbi;
        this.nacionalidadMapper = nacionalidadMapper;
    }

    public List<Nacionalidad> buscarNacionalidades() {
        List<Nacionalidad> listaNacionalidades = new ArrayList<>();

        try (Handle handle = jdbi.open()) {
            listaNacionalidades = handle.createQuery(  "SELECT naccod,nacdsc FROM glm_seguros.NACION" )
                     .map(nacionalidadMapper)
                     .list();
        } catch (Exception e) {
            logger.error("Error SQL consulta localidades", e);
        }
        return listaNacionalidades;
    }

}
