package ar.com.experta.db.mapper;

import ar.com.experta.api.LocalizacionDTO;
import jodd.petite.meta.PetiteBean;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

@PetiteBean
public class LocalizacionDTOMapper implements RowMapper<LocalizacionDTO> {

    @Override
    public LocalizacionDTO map(ResultSet rs, StatementContext ctx) throws SQLException {

        return new LocalizacionDTO(rs.getString("localizacion"),
                                    rs.getString("cp"),
                                    rs.getString("subcp"),
                                    rs.getString("localidad"),
                                    rs.getString("provincia"),
                                    rs.getString("zonahogar"));
    }
}
