package ar.com.experta.db.mapper;

import ar.com.experta.api.LocalizacionDTO;
import ar.com.experta.api.Nacionalidad;
import jodd.petite.meta.PetiteBean;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

@PetiteBean
public class NacionalidadMapper implements RowMapper<Nacionalidad> {

    @Override
    public Nacionalidad map(ResultSet rs, StatementContext ctx) throws SQLException {

        return new Nacionalidad(rs.getString("naccod"),
                                    rs.getString("nacdsc"));
    }
}
