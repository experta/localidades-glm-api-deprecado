package ar.com.experta.exceptions;

public class MapperException extends RuntimeException {
    private int code;
    private String message;

    public MapperException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public MapperException(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
