package ar.com.experta.exceptions.log;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class MethodLoggerAspect {

    private Logger log = Logger.getLogger(MethodLoggerAspect.class);

    @Pointcut("execution(* *(..))")
    public void methodExecutedPointcut() {
    }

    @Around(value = "methodExecutedPointcut()")
    public Object aroundPublicMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        this.log.info(String.format("[m]:%s - args: %s", joinPoint.getSignature(), joinPoint.getArgs()));
        try {
            return joinPoint.proceed();
        } catch (Throwable t) {
            this.log.info(String.format(" - Excepcion en el llamado de <%s>: %s\n", joinPoint.getSignature(), t));
            throw t;
        }
    }

}
