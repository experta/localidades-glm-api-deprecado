package ar.com.experta.resources;

import ar.com.experta.api.LocalizacionDTO;
import ar.com.experta.api.Rama;
import ar.com.experta.core.CodigoPostalService;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@PermitAll
@Path("/codigoPostal")
@Api(value = "Codigo Postal")
@PetiteBean
public class CodigoPostalResource {

    private final CodigoPostalService service;

    @PetiteInject
    public CodigoPostalResource(CodigoPostalService codPostalService) {
        service = codPostalService;
    }

    @GET
    @Timed(name = "time")
    @Metered(name = "metered")
    @ExceptionMetered
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{codigoPostal}/localizaciones")
    @ApiOperation(value = "Buscar localidades por cod. postal", response = LocalizacionDTO.class)
    public Response buscarLocalidadesPorCP(@ApiParam(value = "N° de codigo postal", required = true)
                                         @Valid @PathParam("codigoPostal") Integer idCodPostal)  {
        List<LocalizacionDTO> localidadesDTO = service.buscarLocalizacionPorCP(idCodPostal.toString());
        return Response.ok(localidadesDTO).build();
    }

}
