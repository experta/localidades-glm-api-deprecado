package ar.com.experta.resources;

import ar.com.experta.api.LocalizacionDTO;
import ar.com.experta.core.CodigoPostalService;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@PermitAll
@Path("/localizaciones")
@Api(value = "Localizaciones")
@PetiteBean
public class LocalizacionResource {

    private final CodigoPostalService service;

    @PetiteInject
    public LocalizacionResource(CodigoPostalService codPostalService) {
        service = codPostalService;
    }


    @GET
    @Timed(name = "time")
    @Metered(name = "metered")
    @ExceptionMetered
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    @ApiOperation(value = "Buscar localizaciones por id", response = LocalizacionDTO.class)
    public Response buscarLocalizacionesPorConsecutivo(@ApiParam(value = "Id de localizacion", required = true)
                                           @Valid @PathParam("id") Integer id)  {
        List<LocalizacionDTO> localizacionDTO = service.buscarLocalizacionPorConsecutivo(id.toString());
        return Response.ok(localizacionDTO).build();
    }

}
