package ar.com.experta.resources;

import ar.com.experta.api.LocalizacionDTO;
import ar.com.experta.api.Nacionalidad;
import ar.com.experta.core.CodigoPostalService;
import ar.com.experta.core.NacionalidadService;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@PermitAll
@Path("/nacionalidades")
@Api(value = "Nacionalidades")
@PetiteBean
public class NacionalidadResource {

    private final NacionalidadService service;

    @PetiteInject
    public NacionalidadResource(NacionalidadService nacionalidadService) {
        service = nacionalidadService;
    }


    @GET
    @Timed(name = "time")
    @Metered(name = "metered")
    @ExceptionMetered
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    @ApiOperation(value = "Buscar nacionalidades", response = LocalizacionDTO.class)
    public Response buscarNacionalidad()  {
        List<Nacionalidad> nacionalidades = service.buscarNacionalidades();
        return Response.ok(nacionalidades).build();
    }

}
